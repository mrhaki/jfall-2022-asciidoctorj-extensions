package mrhaki.inline;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Attributes;
import org.asciidoctor.Options;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class MastodonInlineMacroProcessorTest {

    @Test
    void createLinkToMastodonUserPage() {
        try (Asciidoctor asciidoctor = Asciidoctor.Factory.create()) {
            // when
            String result = asciidoctor.convert(
                    "You can follow me at mastodon:mrhaki[].",
                    Options.builder()
                           .inPlace(true)
                           .build()
            );

            // then
            System.out.println(result);
            assertThat(result).contains("<a href=\"https://mastodon.online/@mrhaki\">@mrhaki@mastodon.online</a>.");
        }
    }

    @Test
    void createLinkUsingDocumentAttr() {
        try (Asciidoctor asciidoctor = Asciidoctor.Factory.create()) {
            // when
            String result = asciidoctor.convert(
                    "You can follow me at mastodon:mrhaki[].",
                    Options.builder()
                           .attributes(Attributes.builder()
                                                 .attribute("mastodon-server", "mastodon.social")
                                                 .build())
                           .inPlace(true)
                           .build()
            );

            // then
            System.out.println(result);
            assertThat(result).contains("<a href=\"https://mastodon.social/@mrhaki\">@mrhaki@mastodon.social</a>.");
        }
    }

    @Test
    void createLinkUsingAttr() {
        try (Asciidoctor asciidoctor = Asciidoctor.Factory.create()) {
            // when
            String result = asciidoctor.convert(
                    "You can follow me at mastodon:mrhaki[server=mastodon.server].",
                    Options.builder()
                           .attributes(Attributes.builder()
                                                 .attribute("mastodon-server", "mastodon.social")
                                                 .build())
                           .inPlace(true)
                           .build()
            );

            // then
            System.out.println(result);
            assertThat(result).contains("<a href=\"https://mastodon.server/@mrhaki\">@mrhaki@mastodon.server</a>.");
        }
    }

    @Test
    void createLinkUsingPositionalAttr() {
        try (Asciidoctor asciidoctor = Asciidoctor.Factory.create()) {
            // when
            String result = asciidoctor.convert(
                    "You can follow me at mastodon:mrhaki[mastodon.server].",
                    Options.builder()
                           .attributes(Attributes.builder()
                                                 .attribute("mastodon-server", "mastodon.social")
                                                 .build())
                           .inPlace(true)
                           .build()
            );

            // then
            System.out.println(result);
            assertThat(result).contains("<a href=\"https://mastodon.server/@mrhaki\">@mrhaki@mastodon.server</a>.");
        }
    }
}
