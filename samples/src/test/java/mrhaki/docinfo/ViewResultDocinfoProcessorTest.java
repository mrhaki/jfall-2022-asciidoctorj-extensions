package mrhaki.docinfo;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.junit.jupiter.api.Test;

import java.io.File;

class ViewResultDocinfoProcessorTest {

    @Test
    void generateDocumentation() {
        try (Asciidoctor asciidoctor = Asciidoctor.Factory.create()) {
            // given
            asciidoctor.javaExtensionRegistry().docinfoProcessor(ViewResultDocinfoProcessor.class);

            // when
            asciidoctor.convertFile(
                    new File("viewresult.adoc"),
                    Options.builder()
                           .safe(SafeMode.UNSAFE)
                           .build()
            );
        }
    }
}
