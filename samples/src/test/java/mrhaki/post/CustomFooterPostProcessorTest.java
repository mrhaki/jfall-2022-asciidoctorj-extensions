package mrhaki.post;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.junit.jupiter.api.Test;

import java.io.File;

class CustomFooterPostProcessorTest {

    @Test
    void generate() {
        try (Asciidoctor asciidoctor = Asciidoctor.Factory.create()) {
            // when
            asciidoctor.convertFile(
                    new File("sample.adoc"),
                    Options.builder()
                           .safe(SafeMode.UNSAFE)
                           .build()
            );
        }
    }
}
