package mrhaki.inline;

import org.asciidoctor.ast.ContentNode;
import org.asciidoctor.extension.InlineMacroProcessor;
import org.asciidoctor.extension.Name;
import org.asciidoctor.extension.PositionalAttributes;

import java.util.HashMap;
import java.util.Map;

/**
 * InlineMacroProcessor to turn markup like mastodon:mrhaki[server=mastodon.online].
 * <p>
 * This processor supports the attribute server, which is optional and set
 * the Mastodon server the user is on.
 * The processor will use the document attribute <code>mastodon-server</code> as fallback
 * if the attribute is not set in the markup.
 * If nothing is set the default server is <code>mastodon.online</code>.
 */
@Name("mastodon")
@PositionalAttributes({"server"})
public class MastodonInlineMacroProcessor extends InlineMacroProcessor {
    private static final String DEFAULT_SERVER = "mastodon.online";

    @Override
    public Object process(final ContentNode parent, final String target, final Map<String, Object> attributes) {
        // Try to get document attribute mastodon-server or fallback to default.
        var docAttrServer = (String) parent.getDocument()
                                           .getAttributes()
                                           .getOrDefault("mastodon-server", DEFAULT_SERVER);

        // Get attribute defined in the markup and fallback to document attribute or default value.
        var server = (String) attributes.getOrDefault("server", docAttrServer);

        // Build options map used to create a anchor link.
        Map<String, Object> options = new HashMap<>();
        options.put("target", String.format("https://%s/@%s", server, target));
        options.put("type", ":link");

        // In the link text we include the server and username.
        var linkText = String.format("@%s@%s", target, server);

        // Return a phrase node as this is inline and use context anchor.
        return createPhraseNode(parent, "anchor", linkText, attributes, options);
    }
}
