= Sample view results
:source-highlighter: highlight.js

== Example

Write code so the next `App` can be executed:

[source,java]
.Sample Java app
----
package mrhaki;

public class App {
    // Add your code here
}
----

[source,java,role=result]
----
package mrhaki;

public class App {
    public static void main(String[] args) {
        System.out.println("Hello!");
    }
}
----
