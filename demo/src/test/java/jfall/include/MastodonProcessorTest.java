package jfall.include;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MastodonProcessorTest {
@Test
void generate() {
    try (Asciidoctor asciidoctor = Asciidoctor.Factory.create()) {
        // given
        //asciidoctor.javaExtensionRegistry().inlineMacro(MastodonProcessor.class);   
                
        // when
        String result = asciidoctor.convert(
                """
                        = Sample
                        
                        Hi follow me at mastodon:mrhaki[mastodon.online]""",
                Options.builder()
                       .safe(SafeMode.UNSAFE)
                       .build());

        System.out.println(result);
    }
}

}
