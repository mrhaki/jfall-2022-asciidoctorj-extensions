package jfall.include;

import org.asciidoctor.ast.ContentNode;
import org.asciidoctor.extension.InlineMacroProcessor;
import org.asciidoctor.extension.Name;
import org.asciidoctor.extension.PositionalAttributes;

import java.util.HashMap;
import java.util.Map;

/*
mastodon:mrhaki[]
 */
@Name("mastodon")
@PositionalAttributes({"server"})
public class MastodonProcessor extends InlineMacroProcessor {
    @Override
    public Object process(final ContentNode parent, final String target, final Map<String, Object> attributes) {
        String username = target;
        String server = (String) attributes.getOrDefault("server", "mastodon.online");

        String href = "https://" + server + "/@" + username;
        Map<String, Object> options =new HashMap<>();
        options.put("type", ":link");
        options.put("target", href);
                
        return createPhraseNode(parent, "anchor", "@"+username, attributes, options);
    }
}
