plugins {
    `java-library`
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.asciidoctor:asciidoctorj:2.5.7")
    implementation("org.asciidoctor:asciidoctorj-pdf:2.3.3")
    
    implementation("com.rometools:rome:1.18.0")
    implementation("org.jsoup:jsoup:1.15.3")
    
    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
    testImplementation("org.assertj:assertj-core:3.23.1")
}

tasks.test {
    useJUnitPlatform()
}
